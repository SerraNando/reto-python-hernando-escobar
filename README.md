# Inicialización de la Prueba 

* Se utilizó la tecnología de  [poetry](https://python-poetry.org/docs/) para la creación del entorno

* En la carpeta raíz del proyecto se utiliza el comando ``` poetry shell ``` para inicializar la activación del entorno
 ![poetry shell](/assets/img/poetry_shell.png "poetry shell")

* Ahora crearemos las variables de entorno necesarias para ejecutar **flask** 
    ```
    export FLASK_APP="entrypoint:app"
    export FLASK_ENV="development"
    export APP_SETTINGS_MODULE="config.default"
    export FLASK_DEBUG=1
    ```

* Para la instalación de dependencias ejecutamos ``` poetry install ``` 
 ![poetry install](/assets/img/poetry_install.png "poetry install")

* Ahora inicializamos la base de datos local **flask** 
    ```
    flask db init
    flask db migrate -m "Initial_db"
    flask db upgrade
    ```

* Podríamos ejecutar las pruebas unitarias con ``` flask test ``` 
 ![flask test](/assets/img/poetry_test.png "flask test")

* Por último pondremos en marcha de forma local el proyecto con el comando  ``` flask run ```  y  la documantacion del API  [Swagger](http://127.0.0.1:5000/apidocs/)
 ![flask run](/assets/img/poetry_run.png "flask run")





# ¿Qué repositorio utilizarías?
> Realmente esto es una deciocion que se toma con varias variables entre ellas que funcionamiento tiene la app, uno como desarrollador debe guiar al cliente en tomar la mejor decicion entre funcionamiento, economia y conocieminto yo en lo personal me gusta por enciama de todo  utilizar MongoDB pero no en todas las soluciones e s la mas optima y tampoco es la mas popular en general  