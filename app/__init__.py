from flask import Flask, jsonify
from flask_restful import Api
from flasgger import Swagger
import unittest


from app.common.error_handling import ObjectNotFound, AppErrorBaseClass
from app.db import db
from app.joke.api_v1_0.resources import joke_v1_0_bp
from .ext import ma, migrate


def create_app(settings_module):
    """
    It creates an app, configures it, and returns it
    
    :param settings_module: The name of the settings module to use
    :return: The app object is being returned.
    """
    app = Flask(__name__)
    app.config.from_object(settings_module)

    # Initialize the extensions
    db.init_app(app)
    ma.init_app(app)
    migrate.init_app(app, db)

    # Capture all 404 errors.
    Api(app, catch_all_404s=True)

    # Disables the strict mode of ending a URL with /
    app.url_map.strict_slashes = False

    # Record the blueprints
    app.register_blueprint(joke_v1_0_bp)

    # Register custom error handlers
    register_error_handlers(app)

    # Register swagger
    Swagger(app)
    @app.cli.command()
    def test():
        tests = unittest.TestLoader().discover('tests')
        unittest.TextTestRunner().run(tests)


    return app


def register_error_handlers(app):
    """
    It registers error handlers for the Flask app
    
    :param app: The Flask application instance
    """
    @app.errorhandler(Exception)
    def handle_exception_error(e):
        return jsonify({'msg': 'Internal server error'}), 500

    @app.errorhandler(405)
    def handle_405_error(e):
        return jsonify({'msg': 'Method not allowed'}), 405

    @app.errorhandler(403)
    def handle_403_error(e):
        return jsonify({'msg': 'Forbidden error'}), 403

    @app.errorhandler(404)
    def handle_404_error(e):
        return jsonify({'msg': 'Not Found error'}), 404

    @app.errorhandler(AppErrorBaseClass)
    def handle_app_base_error(e):
        return jsonify({'msg': str(e)}), 500

    @app.errorhandler(ObjectNotFound)
    def handle_object_not_found_error(e):
        return jsonify({'msg': str(e)}), 404
    