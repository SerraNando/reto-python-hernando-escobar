from flask import request, Blueprint
from flask_restful import Api, Resource
from app.common.error_handling import ObjectNotFound
from app.utils.maths import get_mcm_for


from .schemas import JokeSchemas
from ..models import ChuckNorris, Joke
import random


joke_v1_0_bp = Blueprint('joke_v1_0_bp', __name__)

joke_schemas = JokeSchemas()

api = Api(joke_v1_0_bp)


# This endpoint returns a joke based on the type of server sent to it
# You can send the value Chuck and it returns a joke from the test API https://api.chucknorris.io, or
# send Dad and it will return one from https://icanhazdadjoke.com/api
class JokeResourceType(Resource):
    def get(self, type):
        """
        This endpoint returns a joke based on the type of server sent to it
        You can send the value Chuck and it returns a joke from the test API https://api.chucknorris.io, or send Dad and it will return one from https://icanhazdadjoke.com/api
        ---
        parameters:
          - in: path
            name: type
            type: string
            required: true
            default: Chuck | Dad
        responses:
          200:
            description: API default response
            schema:
              id: ChuckNorris
              properties:
                id:
                  type: string
                  description: unique identifier of the joke
                  default: Bn7Xj6otRtOIO5kISAJapg
                value:
                  type: string
                  description: The joke returned by the api
                  default: Chuck Norris cut off his own legs then walked it off
                created_at:
                  type: string
                  description: The creation date of the joke
                  default: 2020-01-05 13:42:30.480041
                icon_url:
                  type: string
                  description: Image of the avatar creator of the joke
                  default: https://assets.chucknorris.host/img/avatar/chuck-norris.png
                updated_at:
                  type: string
                  description: The name type de api server
                  default: 2020-01-05 13:42:30.480041
                url:
                  type: string
                  description: URL API
                  default: https://api.chucknorris.io/jokes/Bn7Xj6otRtOIO5kISAJapg
        """
        response = Joke.get_joke_random(type)
        if response is None:
                raise ObjectNotFound('Server type does not exist')
        return response


# The JokeResource class is a subclass of the Resource class. 
# It has two methods: get and post. 
# The get method returns a random joke from either test API https://api.chucknorris.io,
# https://icanhazdadjoke.com/api. 
# The post method creates a joke in the local database.
class JokeResource(Resource):
    def get(self):
        """
        This endpoint returns a random joke
        random joke from either test API https://api.chucknorris.io, https://icanhazdadjoke.com/api
        ---
        responses:
          200:
            description: API default response
            schema:
              id: ChuckNorris
              properties:
                id:
                  type: string
                  description: unique identifier of the joke
                  default: Bn7Xj6otRtOIO5kISAJapg
                value:
                  type: string
                  description: The joke returned by the api
                  default: Chuck Norris cut off his own legs then walked it off
                created_at:
                  type: string
                  description: The creation date of the joke
                  default: 2020-01-05 13:42:30.480041
                icon_url:
                  type: string
                  description: Image of the avatar creator of the joke
                  default: https://assets.chucknorris.host/img/avatar/chuck-norris.png
                updated_at:
                  type: string
                  description: The name type de api server
                  default: 2020-01-05 13:42:30.480041
                url:
                  type: string
                  description: URL API
                  default: https://api.chucknorris.io/jokes/Bn7Xj6otRtOIO5kISAJapg
        """
        types = ['Chuck', 'Dad', 'Chuck', 'Dad', 'Chuck',
                 'Dad', 'Chuck', 'Dad', 'Chuck', 'Dad']

        for type_random in range(1):
            type_random = types[random.randint(0, 9)]
            response = ChuckNorris.get_joke_random(type_random)
            if response is None:
                raise ObjectNotFound('Server type does not exist')
            return response

    def post(self):
        """
        This endpoint create a joke in local database
        the value parameter is passed with the joke that you want to create in the database
        ---
        parameters:
          - in: body
            name: value
            type: string
            required: true
            default: Chuck Norris cut off his own legs then walked it off
        responses:
          201:
            description: API default response
            schema:
              id: Joke
              properties:
                id:
                  type: string
                  description: unique identifier of the joke
                  default: Bn7Xj6otRtOIO5kISAJapg
                value:
                  type: string
                  description: The joke returned by the api
                  default: Chuck Norris cut off his own legs then walked it off
        """
        data = request.get_json()
        joke_dict = joke_schemas.load(data)
        joke = Joke(value=joke_dict['value'])
        joke.save()
        resp = joke_schemas.dump(joke)
        return resp, 201


# This class is a subclass of the Resource class from Flask-RESTful. 
# It defines the HTTP methods that can be used to access the resource, 
# and the logic that will be executed when each method is used
class JokeByResource(Resource):
    def get(self, number):
        """
        This endpoint get a joke from the local database
        the number parameter to get a database joke
        ---
        parameters:
          - in: path
            name: number
            type: string
            required: true
            default: 1
        responses:
          200:
            description: API default response
            schema:
              id: Joke
              properties:
                id:
                  type: string
                  description: unique identifier of the joke
                  default: Bn7Xj6otRtOIO5kISAJapg
                value:
                  type: string
                  description: The joke returned by the api
                  default: Chuck Norris cut off his own legs then walked it off
        """
        joke = Joke.get_by_id(number)
        if joke is None:
            raise ObjectNotFound('Joke does not exist')
        resp = joke_schemas.dump(joke)
        return resp

    def delete(self, number):
        """
        This endpoint delete a joke from the local database
        the number parameter to delete a database joke
        ---
        parameters:
          - in: path
            name: number
            type: string
            required: true
            default: 1
        responses:
          204:
            description: API default response
            schema:
              id: Joke
              properties:
                id:
                  type: string
                  description: unique identifier of the joke
                  default: Bn7Xj6otRtOIO5kISAJapg
                value:
                  type: string
                  description: The joke returned by the api
                  default: Chuck Norris cut off his own legs then walked it off
        """
        joke = Joke.get_by_id(number)
        print('joke', joke)
        if joke is None:
            raise ObjectNotFound('Joke does not exist')
        joke.delete()
        return '', 204

    def put(self, number):
        """
        This endpoint update a joke from the local database
        the number parameter to update a database joke
        ---
        parameters:
          - in: path
            name: number
            type: string
            required: true
            default: 1
          - in: body
            name: value
            type: string
            required: true
            default: Chuck Norris cut off his own legs then walked it off  
        responses:
          201:
            description: API default response
            schema:
              id: Joke
              properties:
                id:
                  type: string
                  description: unique identifier of the joke
                  default: Bn7Xj6otRtOIO5kISAJapg
                value:
                  type: string
                  description: The joke returned by the api
                  default: Chuck Norris cut off his own legs then walked it off
        """
        data = request.get_json()
        joke = Joke.get_by_id(number)
        joke.value = data['value']
        print('data', joke)

        if joke is None:
            raise ObjectNotFound('Joke does not exist')
        joke.update()
        resp = joke_schemas.dump(joke)
        return resp, 201


# This class is a subclass of Resource, and it has a get method that returns a dictionary with a
# message key and a value that is a string.
class MCMResource(Resource):
    def get(self):
        """
        This endpoint of a mathematical operation to obtain the mcm
        You will pass a list of numbers separated by commas in the query with name numbers and it will return its result
        ---
        parameters:
          - in: query
            name: numbers
            type: string
            required: true
            default: 1,30,40
        responses:
          200:
            description: API default response
            schema:
              id: Maths
              properties:
                message:
                  type: string
                  description: message with response of mathematical operation
                  default: El mínimo común múltiplo de [1, 90, 300, 10000] es 90000
        """
        numbers = request.args
        output = numbers['numbers'].split(',')
        numbers_list = []
        for x in output:
            numbers_list.append(int(x))
        response = get_mcm_for(numbers_list)
        return {'message': 'El mínimo común múltiplo de {} es {}'.format(numbers_list, response)}


# A class that inherits from the Resource class of the Flask-RESTful library, it has a method called
# get that returns a dictionary with the result of the mathematical operation
class IntegerResource(Resource):
    def get(self):
        """
        This endpoint of a mathematical operation to obtain sum integer numbers
        You will pass a list of numbers separated by commas in the query with name numbers and it will return its result
        ---
        parameters:
          - in: query
            name: number
            type: string
            required: true
            default: 10
        responses:
          200:
            description: API default response
            schema:
              id: Maths
              properties:
                message:
                  type: string
                  description: message with response of mathematical operation
                  default: El mínimo común múltiplo de [1, 90, 300, 10000] es 90000
        """
        number = request.args['number']
        if '.' in number:
            return  {'message': 'The number you passed is float ({}), it should be an integer'.format(number)}, 400

        else:
            response = int(number)+1
            return {'message': 'The number you passed is {} sum with the number 1 gives {}'.format(number, response)}


api.add_resource(JokeResource, '/api/v1.0/joke/',
                 endpoint='joke_resource')
api.add_resource(JokeResourceType, '/api/v1.0/joke/<string:type>',
                 endpoint='joke_type_resource')
api.add_resource(JokeByResource, '/api/v1.0/joke/<int:number>',
                 endpoint='joke_by_resource')

api.add_resource(MCMResource, '/api/v1.0/maths/mcm',
                 endpoint='mcm_resource')

api.add_resource(IntegerResource, '/api/v1.0/maths/integer',
                 endpoint='integer_resource')
