from marshmallow import fields

from app.ext import ma

# `JokeSchemas` is a class that inherits from `ma.Schema` and has two fields: `id` and `value`

class JokeSchemas(ma.Schema):
    id = fields.Integer(dump_only=True)
    value = fields.String()

