from app.db import db, BaseModelMixin

# A Joke is a string that is stored in the database.

class Joke(db.Model, BaseModelMixin):
    id = db.Column(db.Integer, primary_key=True)
    value = db.Column(db.String)

    def __init__(self, value):
        self.value = value

    def __repr__(self):
        return f'Joke({self.value})'

    def __str__(self):
        return f'{self.value}'


# It's a class that inherits from two other classes, and has a bunch of attributes
class ChuckNorris(db.Model, BaseModelMixin):
    id = db.Column(db.Integer, primary_key=True)
    value = db.Column(db.String)
    created_at = db.Column(db.Date)
    icon_url = db.Column(db.String)
    updated_at = db.Column(db.Date)
    url = db.Column(db.String)

    def __init__(self, value, created_at, icon_url, updated_at, url):
        self.value = value
        self.created_at = created_at
        self.icon_url = icon_url
        self.updated_at = updated_at
        self.url = url

    def __repr__(self):
        return f'ChuckNorris({self.value})'

    def __str__(self):
        return f'{self.value}'
