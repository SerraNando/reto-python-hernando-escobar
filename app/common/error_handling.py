# It's a base class for all exceptions that are raised by the application.
class AppErrorBaseClass(Exception):
    pass

# This is an error that occurs when an object is not found.

class ObjectNotFound(AppErrorBaseClass):
    pass