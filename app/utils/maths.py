from functools import reduce

def mcm(a, b):
    """
    It returns the least common multiple of two numbers
    
    :param a: the first number
    :param b: the number of columns in the grid
    """
    if a > b:
        greater_than = a
    else:
        greater_than = b

    while True:
        if greater_than % a == 0 and greater_than % b == 0:
            mcm = greater_than
            break
        greater_than += 1

    return mcm

def get_mcm_for(stored_list):
    """
    It takes a list of numbers and returns the least common multiple of all the numbers in the list
    
    :param stored_list: a list of numbers
    :return: The least common multiple of all the numbers in the list.
    """
    return reduce(lambda x, y: mcm(x, y), stored_list)
