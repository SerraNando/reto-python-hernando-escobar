from flask_sqlalchemy import SQLAlchemy
import requests


# Initialize the extensions db
db = SQLAlchemy()


# This class provides a method to serialize a model instance into a dictionary.
class BaseModelMixin:

    def save(self):
        db.session.add(self)
        db.session.commit()
        
    def update(self):
        # db.session.update(self)
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    @classmethod
    def get_all(cls):
        return cls.query.all()

    @classmethod
    def get_by_id(cls, id):
        return cls.query.get(id)

    @classmethod
    def simple_filter(cls, **kwargs):
        return cls.query.filter_by(**kwargs).all()

    """
    *|CURSOR_MARCADOR|*
    :param cls: The class itself
    :param type: The type of joke to return. Valid types are:
    """
    @classmethod
    def get_joke_random(cls, type):
        if 'Chuck' in type:
            url = "https://api.chucknorris.io/jokes/random"

            payload = ""
            response = requests.request("GET", url, data=payload)
            if response.ok:
                return response.json()
        elif 'Dad' in type:
            url = "https://icanhazdadjoke.com/"

            payload = ""
            headers = {"Accept": "application/json"}

            response = requests.request(
                "GET", url, data=payload, headers=headers)
            if response.ok:
                return response.json()
        else:
            return None
