from flask_testing import TestCase
from flask import current_app, Flask
from flask_restful import Api


class MainTest(TestCase):
    def create_app(self):
        self.app = Flask(__name__)

        self.app.config['TESTING'] = True
        self.app.config['WTF_CSRF_ENABLED'] = False
        self.app.test_client()

        Api(self.app)

        return self.app

    def test_app_exists(self):
        self.assertIsNotNone(current_app)

    def test_app_in_test_mode(self):
        self.assertTrue(current_app.config['TESTING'])

    def test_joke_get(self):
        response = self.client.get('/api/v1.0/joke/')
        self.assert200(response)

    def test_joke_get_type(self):
        fake_path = 'Chuck'
        response = self.client.get(f'/api/v1.0/joke/{fake_path}')
        self.assert200(response)

    def test_joke_post(self):
        fake_form = {
            'value': 'fake joke',
        }
        response = self.client.post('/api/v1.0/joke/', data=fake_form)
        self.assert201(response)

    def test_joke_get_by(self):
        fake_path = '1'
        response = self.client.get(f'/api/v1.0/joke/{fake_path}')
        self.assert200(response)

    def test_joke_update(self):
        fake_path = '1'
        fake_form = {
            'value': 'fake joke update',
        }
        response = self.client.post(f'/api/v1.0/joke/{fake_path}', data=fake_form)
        self.assert201(response)

    def test_joke_delete(self):
        fake_path = '1'
        response = self.client.post(f'/api/v1.0/joke/{fake_path}')
        self.assert204(response)


